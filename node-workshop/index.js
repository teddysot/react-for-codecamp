const fs = require('fs');
const os = require('os');

const data = fs.readFileSync('./sample.json')
const jsonObj = JSON.parse(data)
const users = jsonObj.users
let newObj = []
users.forEach(e => {
    newObj.push(`${e.firstName} ${e.lastName} ${e.phoneNumber} ${e.emailAddress}`)
});
const newData = JSON.stringify({ ...newObj })
fs.writeFileSync('./output.json', newData);

// const data = fs.readFileSync('readme.txt', 'utf-8')

// console.log(data);

// fs.readFile('readme.txt', (err, data) => {
//     if (err) {
//         console.log(err);
//         return;
//     }
//     console.log(data.toString());
// })

// let content = 'System Info\n'
// content += os.cpus()[0].model;
// content += '\nTotal Memory : '
// content += os.totalmem();

// fs.writeFile('output.txt', content, (err) => {
//     if (err) {
//         console.log(err);
//     }
// })