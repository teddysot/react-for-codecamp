const { greet } = require('../app/index')

test('Test requirement1', () => {
    expect(greet("Sonter")).toBe("Hello, Sonter")
})