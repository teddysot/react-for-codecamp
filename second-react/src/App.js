import React, { Component } from 'react';
import './App.css';
import PersonalCardList from './components/PersonalCardList';
import { WrappedPersonalList } from './components/PersonalCardList'
import RenderProps from './components/RenderProps'
import Counter from './components/Counter'
import Carousel from './components/Carousel';
import TabComponent from './components/TabComponent';
import Tab from './components/Tab'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
      announcement: "This is announcement",
      isShow: true,
      stds: [
        {
          "name": "Ivan Meyer",
          "email": "Sed.nunc.est@Pellentesquehabitant.ca",
          "age": 79
        },
        {
          "name": "Gareth Fuller",
          "email": "pretium.neque@MorbivehiculaPellentesque.com",
          "age": 64
        },
        {
          "name": "Henry Weber",
          "email": "Suspendisse.commodo@mauriseuelit.ca",
          "age": 45
        }
      ],
      students:
        [
          {
            name: "GGWP",
            age: 19,
            color: "Red"
          },
          {
            name: "Tle",
            age: 21,
            color: "Green"
          },
          {
            name: "Sonter",
            age: 24,
            color: "Blue"
          },
        ]
    }
  }

  static getDerivedStateFromProps(props, state) {
    return state;
  }

  setName = (name) => {
    this.setState({ announcement: name })
  }

  setCounter = (value) => {
    this.setState({ counter: this.state.counter + value })
  }
  render() {

    return (
      <div className="App">
        {/* <button onClick={() => this.setState({ isShow: !this.state.isShow })}>{this.state.isShow ? "Hide All Students" : "Show All Students"}</button>
        {this.state.isShow ? this.state.students
          .map((student, idx) => (
            <PCComponent
              key={idx}
              name={student.name}
              age={student.age}
              color={student.color} />
          )) : null} */}
        <TabComponent>
          <Tab name="ONE" bgColor="salmon" />
          <Tab name="TWO" bgColor="pink" />
          <Tab name="THREE" bgColor="lime" />
        </TabComponent>
        <Carousel />
        <RenderProps
          render={() => (
            <Counter
              counter={this.state.counter}
              setCounter={this.setCounter}
            />
          )}
        />
        <h1>{this.state.announcement}</h1>
        <input
          value={this.state.announcement}
          onChange={(e) => this.setState({ announcement: e.target.value })}
          style={{ fontSize: "50px" }} />
        <button onClick={() => this.setState({ announcement: "" })} style={{ fontSize: "50px" }}>RESET</button>
        <button onClick={() => this.setState({ isShow: !this.state.isShow })} style={{ fontSize: "50px" }}>{this.state.isShow ? "Hide" : "Show"}</button>
        <table style={{ width: "100%", fontSize: "24px" }}>
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Age</th>
              <th>Announcement</th>
            </tr>
          </thead>
          <tbody>
            {this.state.isShow && this.state.stds.map((student, idx) => (
              <WrappedPersonalList
                key={idx}
                name={student.name}
                email={student.email}
                age={student.age}
                setName={this.setName}
              />
            ))}
            {this.state.isShow && this.state.stds.map((student, idx) => (
              <PersonalCardList
                key={idx}
                name={student.name}
                email={student.email}
                age={student.age}
                setName={this.setName}
              />
            ))}
            {this.state.isShow && this.state.stds.map((student, idx) => (
              <RenderProps
                key={idx}
                render={(fn) => (
                  <PersonalCardList
                    key={idx}
                    name={student.name}
                    email={student.email}
                    age={student.age}
                    setName={this.setName}
                    alertSomething={fn}
                  />
                )}
              />
            ))}
          </tbody>
        </table>
      </div >
    );
  }
}

export default App;
