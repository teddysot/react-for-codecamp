import React, { Component } from 'react'

export default class RenderProps extends Component {
    render() {
        return (
            <>
                { this.props.render()}
            </>
        )
    }
}
