import React, { Component } from 'react'
import { AddHeader } from './AddHeader'
import PropTypes from 'prop-types'

export default class PersonalCardList extends Component {

    // static getDerivedStateFromProps(props, state) {

    // }

    // shouldComponentUpdate(nextProps, nextState) {

    // }

    render() {
        let bgColor = "white"
        let result = this._reactInternalFiber.key % 3
        switch (result) {
            case 0:
                bgColor = "lime"
                break;
            case 1:
                bgColor = "salmon"
                break;
            case 2:
                bgColor = "pink"
                break;
            default:
                break;
        }
        return (
            <tr style={{ backgroundColor: bgColor }}>
                <td>{this.props.name}</td>
                <td>{this.props.email}</td>
                <td>{this.props.age}</td>
                <td><button onClick={() => { this.props.setName(this.props.name) }}>Call My Name!!</button>
                    <button onClick={this.props.alertSomething}>Alert</button>
                </td>
            </tr>
        )
    }

    // getSnapshotBeforeUpdate(prevProps, prevState) {

    // }

    // componentDidUpdate() {

    // }
}

PersonalCardList.propTypes = {
    name: PropTypes.string,
    email: PropTypes.string,
    age: PropTypes.number
}

export const WrappedPersonalList = AddHeader(PersonalCardList)