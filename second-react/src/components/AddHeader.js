import React, { Component } from 'react'

export const AddHeader = WrappedComponent => {
    return class extends Component {
        render(){
            return (
                <WrappedComponent {...this.props} alertSomething={() => alert("Google")}/>
            )
        }
    }
}