import React from 'react'

function PersonalCard(props) {
    return (
        <div style={{ border: "1px solid", margin: "20px", borderColor: props.color }}>
            <p>Name: {props.name}</p>
            <p>Age: {props.age}</p>
            <p>Color: {props.color}</p>
        </div>
    )
}

export default PersonalCard
