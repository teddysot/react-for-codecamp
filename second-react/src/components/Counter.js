import React, { Component } from 'react'

export default class Counter extends Component {
    render() {
        return (
            <>
                <button onClick={() => this.props.setCounter(1)}>+</button>
                <span> {this.props.counter} </span>
                <button disabled={this.props.counter === 0 ? true : false} onClick={() => {
                    if (this.props.counter > 0) {
                        this.props.setCounter(-1)
                    }
                }}>-</button>
            </>
        )
    }
}
