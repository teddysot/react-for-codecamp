import React, { Component } from 'react'

export default class Input extends Component {

    getClassCSS = () => {
        let cssClass = "Input InputElement"

        if (this.props.error.status && this.props.error.isTouched) {
            cssClass += " Invalid"
        }

        return cssClass;
    }

    render() {
        const { value, onChangeValue, type, name, placeholder, error } = this.props
        return (
            <>
                <input value={value}
                    onChange={onChangeValue}
                    type={type}
                    name={name}
                    className={this.getClassCSS()}
                    placeholder={placeholder}
                    error={error} />
                <p className="ErrorMessage">{this.props.error.message}</p>
            </>
        )
    }
}
