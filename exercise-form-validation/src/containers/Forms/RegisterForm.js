import React, { Component } from 'react'
import Input from '../Input/Input'
const _ = require('lodash');

export default class RegisterForm extends Component {

    formStructure = {
        formData: {
            name: {
                value: "",
                validator: {
                    minLength: 3,
                    maxLength: 12,
                    required: true
                },
                error: {
                    status: true,
                    message: "",
                    isTouched: false
                }
            },
            phonenumber: {
                value: "",
                validator: {
                    minLength: 10,
                    maxLength: 10,
                    required: true
                },
                error: {
                    status: true,
                    message: "",
                    isTouched: false
                }
            },
            email: {
                value: "",
                validator: {
                    required: true
                },
                error: {
                    status: true,
                    message: "",
                    isTouched: false
                }
            },
            password: {
                value: "",
                validator: {
                    minLength: 6,
                    maxLength: 24,
                    required: true
                },
                error: {
                    status: true,
                    message: "",
                    isTouched: false
                }
            },
        },
        isFormValid: false,
    }

    state = _.cloneDeep(this.formStructure)

    resetForm = () => {
        this.setState(_.cloneDeep(this.formStructure))
    }

    onSubmitForm = (e) => {
        e.preventDefault()
        const { name, phonenumber, email, password } = this.state.formData

        const result = JSON.stringify(
            {
                name: name.value,
                phonenumber: phonenumber.value,
                email: email.value,
                password: password.value
            })

        alert(result)

        this.resetForm();
    }


    checkValue = (value, rule) => {
        let isValid = true;
        let trimmedValue = value.trim();
        let message = "";

        if (rule.required && trimmedValue.length === 0) {
            isValid = false;
            message = "You need to fill this section!"
        }

        if (rule.maxLength && trimmedValue.length > rule.maxLength) {
            isValid = false;
            message = `Input must be less than ${rule.maxLength} letters`
        }

        if (rule.minLength && trimmedValue.length < rule.minLength) {
            isValid = false;
            message = `Input must be at least ${rule.minLength} letters`
        }

        return { isValid, message };
    }

    onChangeValue = (e) => {
        const fieldName = e.target.name;
        const fieldValue = e.target.value;
        const formUpdate = { ...this.state.formData };
        formUpdate[fieldName].value = fieldValue;
        formUpdate[fieldName].error.isTouched = true;

        let { isValid, message } = this.checkValue(e.target.value, formUpdate[fieldName].validator)

        formUpdate[fieldName].error.status = !isValid;
        formUpdate[fieldName].error.message = message;

        let newIsFormValid = true;

        for (const key in formUpdate) {
            if (formUpdate[key].validator.required) {
                newIsFormValid = !formUpdate[key].error.status && newIsFormValid;
            }
        }

        this.setState({
            formData: formUpdate,
            isFormValid: newIsFormValid
        })
    }

    render() {
        const { name, phonenumber, email, password } = this.state.formData
        return (
            <div className="RegisterForm">
                <form action="" onSubmit={this.onSubmitForm}>
                    <Input onChangeValue={this.onChangeValue} type="text" value={name.value} name="name" placeholder="Name" error={name.error} />
                    <Input onChangeValue={this.onChangeValue} type="tel" value={phonenumber.value} name="phonenumber" placeholder="Phone Number" error={phonenumber.error} />
                    <Input onChangeValue={this.onChangeValue} type="email" value={email.value} name="email" placeholder="Email" error={email.error} />
                    <Input onChangeValue={this.onChangeValue} type="password" value={password.value} name="password" placeholder="Password" error={password.error} />
                    <button disabled={!this.state.isFormValid} className="Button">Register</button>
                </form>
            </div>
        )
    }
}
