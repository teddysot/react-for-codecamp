const express = require('express');
const cors = require('cors')

const todoRouter = require('./routes/todos')

const app = express();

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: false }))
app.use(express.static('./public/'))

app.use('/todos', todoRouter)

app.use((req, res, next) => {
    next(new Error('Path not found'))
})

app.use((err, req, res, next) => {
    res.status(404).send(err.message)
})

const port = 80;
app.listen(port, () => {
    console.log(`server starting on port ${port}`);
});