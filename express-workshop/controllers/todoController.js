const fs = require('fs');

const filePath = './mocks/todo.json'

const todoLists = JSON.parse(fs.readFileSync(filePath));

exports.getTodos = (req, res) => {
    const searchTask = req.query.search;

    if (searchTask) {
        const filteredList = todoLists.filter((e) => e.task.includes(searchTask))
        return res.status(200).send(filteredList)
    }

    res.status(200).send(todoLists);
};

exports.validateId = (req, res, next) => {
    const id = +req.params.id;
    const idx = todoLists.findIndex(el => el.id === id);

    if (idx === -1) {
        next(new Error('Invalid Id'))
    }

    req.idx = idx
    req.id = id
    next();
}

exports.getTodo = (req, res) => {
    const todo = todoLists.filter(el => el.id === req.id);
    res.status(200).send({ todo: todo[0] });
};

exports.createTodo = (req, res) => {
    const newId = todoLists.length > 0 ? todoLists[todoLists.length - 1].id + 1 : 0;
    console.log(req.body);
    const newTodoList = {
        id: newId, task: req.body.task
    };

    todoLists.push(newTodoList);

    fs.writeFileSync(filePath, JSON.stringify(todoLists));
    res.status(201).send({ todo: newTodoList });
};

exports.updateTodo = (req, res) => {
    todoLists[req.idx] = { id: req.id, task: req.body.task };

    fs.writeFileSync(filePath, JSON.stringify(todoLists));
    res.status(200).send({ todo: todoLists });
};

exports.deleteTodo = (req, res) => {
    const newTodoLists = todoLists.filter(el => el.id !== req.id);
    fs.writeFileSync(filePath, JSON.stringify(newTodoLists));
    res.status(204).send();
};