const express = require('express')

const app = express();

app.use(express.json());

const port = 5555;

// Start server function
app.listen(port, () => {
    console.log(`Server starting on port ${port}`);
})

app.get('/bye', (req, res) => {
    res.status(200).send("Good Bye")
    console.log('[GET] Received');
});

app.post('/bye/:task', (req, res) => {
    console.log(`[POST] ${req.params.task}`);
    res.status(201).send("[POST] Success")
    console.log('[POST] Received');
});

app.put('/bye/:task', (req, res) => {
    console.log(`[PUT] ${req.params.task}`);
    res.status(201).send("[PUT] Success")
    console.log('[PUT] Received');
});

app.delete('/bye/:task', (req, res) => {
    console.log(`[DELETE] ${req.params.task}`);
    res.status(204).send()
    console.log('[DELETE] Received');
});