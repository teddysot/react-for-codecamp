import React, { Component } from 'react';
import './App.css';
import TimeZoneClock from './components/TimeZoneClock'
import Container from 'react-bootstrap/Container'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

class App extends Component {

  constructor() {
    super()

    this.state = {
      continent: "Asia",
      city: "Bangkok"
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();

    this.setState({ continent: document.getElementById("continent").value })
    this.setState({ city: document.getElementById("city").value })
  }

  render() {
    return (
      <div className="App" >
        <Jumbotron style={{
          display: "flex",
          backgroundColor: "salmon",
          width: "300px",
          height: "60px",
          justifyContent: "center",
          alignItems: "center",
          margin: "0 auto"
        }}>
          <Container style={{
            display: "flex"
          }}>
            <div style={{
              display: "flex",
              fontWeight: "bold"
            }}>World Clock</div>
          </Container>
        </Jumbotron>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="continent">
            <Form.Label>Enter Continent </Form.Label>
            <Form.Control type="text" placeholder="Asia" />
          </Form.Group>
          <Form.Group controlId="city">
            <Form.Label>Enter City </Form.Label>
            <Form.Control type="text" placeholder="Bangkok" />
          </Form.Group>
          <Button type="submit">Change Time</Button>
        </Form>
        <TimeZoneClock location={this.state.city} timezone={this.state.continent + '/' + this.state.city} bgColor="darkgray" />
        <TimeZoneClock timezone='Asia/Bangkok' bgColor="pink" />
        <TimeZoneClock timezone='Europe/London' bgColor="lime" />
        <TimeZoneClock timezone='Asia/Tokyo' bgColor="skyblue" />
      </div>
    );
  }
}

export default App;
