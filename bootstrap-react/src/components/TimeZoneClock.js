import React, { Component } from 'react'
import Container from 'react-bootstrap/esm/Container';
import Jumbotron from 'react-bootstrap/esm/Jumbotron';

// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
export default class TimeZoneClock extends Component {
    constructor(props) {
        super(props)
        this.state = {
            time: 0
        }
    }

    tick() {
        this.setState({
            time: new Date().toLocaleString('en-US', { timeZone: this.props.timezone })
        })
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        let time = this.state.time
        time = String(time).split(', ')
        return (
            <div style={{ margin: "10px" }}>
                <Jumbotron style={{
                    display: "flex",
                    backgroundColor: this.props.bgColor,
                    width: "300px",
                    height: "60px",
                    justifyContent: "center",
                    alignItems: "center",
                    margin: "0 auto",
                    borderRadius: "10px"
                }}>
                    <Container style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center"
                    }}>
                        <div style={{
                            display: "flex",
                            fontWeight: "bold"
                        }}>{String(this.props.timezone).split('/')[1]}</div>
                        <div style={{ display: "flex" }}>{time[0]}</div>
                        <div style={{ display: "flex" }}>{time[1]}</div>
                    </Container>
                </Jumbotron>
            </div>
        )
    }
}
