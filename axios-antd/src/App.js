import React, { Component } from 'react';
import { Table } from 'antd';
import './App.css';
import axios from 'axios'

export default class App extends Component {
  state = {
    data: [],
    loading: true
  }

  fetchData = async () => {
    const res = await axios.get('https://run.mocky.io/v3/c7a55023-ded8-439f-8452-d004d83c3354?mocky-delay=3000ms')
    this.setState({
      data: res.data,
      loading: false
    });
  }

  componentDidMount() {
    this.fetchData();
  }
  render() {
    // console.log(this.data);
    const columns = [
      {
        title: "ชื่อ",
        dataIndex: "name"
      },
      {
        title: "วันเกิด",
        dataIndex: "birthday"
      },
      {
        title: "บริษัท",
        dataIndex: "company"
      },
      {
        title: "เบอร์โทรศัพท์",
        dataIndex: "phoneNumber"
      }
    ];
    return (
      <div className="App">
        <Table columns={columns} dataSource={this.state.data} size="middle" loading={this.state.loading} />
      </div>
    )
  }
}


// import React, { useState, useEffect } from 'react';
// import { Table } from 'antd'
// import axios from 'axios'
// import './App.css';

// export default function App() {
//   const [data, setData] = useState([])

//   const columns = [
//     {
//       title: "ชื่อ",
//       dataIndex: "name",
//       render: (text =>
//         <a>{text}</a>
//       )
//     },
//     {
//       title: "วันเกิด",
//       dataIndex: "birthday"
//     },
//     {
//       title: "บริษัท",
//       dataIndex: "company",
//       render: ((text, record) => 
//         <a>{text} ({record.name})</a>
//       )
//     },
//     {
//       title: "เบอร์โทร",
//       dataIndex: "phoneNumber"
//     }
//   ]

//   useEffect(() => {
//     axios.get('https://run.mocky.io/v3/c7a55023-ded8-439f-8452-d004d83c3354')
//       .then(res => {
//         setData(res.data)
//       })
//   }, [])

//   return (
//     <div className="App">
//       <Table columns={columns} dataSource={data} />
//     </div>
//   );
// }