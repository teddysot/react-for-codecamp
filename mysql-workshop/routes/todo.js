const router = require('express').Router();
const passport = require('passport')
const todoController = require('../controllers/todoController.js')

const auth = passport.authenticate('jwt', { session: false })

router.get('/', auth, todoController.getTodos);
router.get('/:id', auth, todoController.getTodo);
router.post('/', auth, todoController.createTodo);
router.put('/:id', auth, todoController.updateTodo);
router.delete('/:id', auth, todoController.deleteTodo);

module.exports = router;