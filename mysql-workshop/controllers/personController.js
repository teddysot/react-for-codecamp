const db = require("../models");

exports.getAllPersons = async (req, res) => {
    const allPersons = await db.Person.findAll({ include: [db.Todo] });
    res.status(200).send(allPersons);
}

exports.getPerson = async (req, res) => {
    const targetId = req.params.id
    const targetPerson = await db.Person.findOne({ where: { id: targetId }, include: [db.Todo] })
    res.status(200).send(targetPerson)
}

exports.addPerson = async (req, res) => {
    const { name, age, todos } = req.body;
    const newPerson = await db.Person.create(
        {
            name,
            age,
            Todos: todos
        }, {
        include: [db.Todo]
    })
    res.status(201).send(newPerson)
}

exports.updatePerson = async (req, res) => {
    const { name, age } = req.body;
    const targetId = req.params.id
    const targetPerson = await db.Person.findOne({ where: { id: targetId } })

    if (targetPerson) {
        await targetPerson.update({ name, age })
        res.status(201).send(newPerson)
    }
    else {
        res.status(404).send()
    }
}

exports.deletePerson = async (req, res) => {
    const targetId = req.params.id
    const targetPerson = await db.Person.findOne({ where: { id: targetId } })

    if (targetPerson) {
        await targetPerson.destroy()
        res.status(204).send()
    }
    else {
        res.status(404).send()
    }
}