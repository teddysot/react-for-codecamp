const db = require("../models")

exports.getTodos = async (req, res) => {
    const allTodos = await db.Todo.findAll({ where: { person_id: req.user.id } });
    res.status(200).send(allTodos)
};

exports.getTodo = async (req, res) => {
    const targetId = req.params.id
    const targetTodo = await db.Todo.findOne({ where: { id: targetId } })
    res.status(200).send(targetTodo)
};

exports.createTodo = async (req, res) => {
    const { task } = req.body;
    const newTodo = await db.Todo.create({ task, person_id: req.user.id })

    res.status(201).send(newTodo)
};

exports.updateTodo = async (req, res) => {
    const targetId = req.params.id
    const { task } = req.body;
    const targetTodo = await db.Todo.findOne({ where: { id: targetId } })

    if (targetTodo && targetTodo.person_id === req.user.id) {
        targetTodo.update({ task })
        res.status(201).send(targetTodo)
    } else {
        res.status(404).send({ message: `Not found ID: ${targetId}` })
    }
};

exports.deleteTodo = async (req, res) => {
    const targetId = req.params.id
    const targetTodo = await db.Todo.findOne({ where: { id: targetId } })

    if (targetTodo && targetTodo.person_id === req.user.id) {
        targetTodo.destroy();
        res.status(200).send({ message: `ID: ${targetId} has been deleted` })
    } else {
        res.status(404).send({ message: `Not found ID: ${targetId}` })
    }
};