const db = require("../models");
const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken');
const e = require("express");
const { ExtractJwt } = require("passport-jwt");

exports.register = async (req, res) => {
    const { username, password, name, age } = req.body;
    const targetUser = await db.Person.findOne({ where: { username } })

    if (targetUser) {
        res.status(400).send({ message: "Username already taken" });
    }
    else {
        bcryptjs.genSalt(12, (err, salt) => {
            if (err) {
                res.status(400).send({ message: "Signup Failed" });
            }

            bcryptjs.hash(password, salt, async (err, hash) => {
                if (err) {
                    res.status(400).send({ message: "Signup Failed" });
                }

                await db.Person.create({
                    username,
                    name,
                    age,
                    password: hash
                })

                res.status(201).send({ message: "Signup Successfully" })
            })
        })
    }
}

exports.login = async (req, res) => {
    const { username, password } = req.body
    const targetUser = await db.Person.findOne({ where: { username } })

    if (!targetUser) {
        res.status(400).send({ message: "Authentication Failed" })
    }
    else {
        bcryptjs.compare(password, targetUser.password, (err, isCorrect) => {
            if (err) {
                res.status(400).send({ message: "Authentication Failed" })
            }
            if (isCorrect) {
                const payload = {
                    id: targetUser.id,
                    name: targetUser.name
                }

                const token = jwt.sign(payload, "teddysot", { expiresIn: 3600 })
                res.status(200).send({ token })
            } else {
                res.status(400).send({ message: "Authentication Failed" })
            }
        });
    }
}