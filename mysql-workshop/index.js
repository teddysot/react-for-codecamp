const express = require('express');
const app = express();
const db = require('./models')
const personRoute = require('./routes/person')
const todoRoute = require('./routes/todo')
const userRoute = require('./routes/user')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

require('./config/passport')
app.use('/auth', userRoute)
app.use("/todos", todoRoute)
app.use('/persons', personRoute)

const port = 80
app.listen(port, () => {
    console.log("Server starting at port: " + port);
})

db.sequelize.sync({ force: false }).then(() => {
    console.log('Database established');
}).catch(err => {
    console.log(err);
})