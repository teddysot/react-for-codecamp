import React, { useState, useEffect } from 'react';
import { Col, Row, List, Input, Button } from 'antd';
import TodoItem from './components/TodoItem';
import axios from 'axios'

const App = () => {
  const [loading, setLoading] = useState(true)
  const [todoList, setTodoList] = useState([]);
  const [inputValue, setInputValue] = useState("");

  const fetchTodos = async () => {
    const res = await axios.get('http://localhost/todos')
    setTodoList(res.data)
    setLoading(false)
  }

  useEffect(() => {
    fetchTodos()
  }, [])

  const addTodo = () => {
    const task = inputValue;
    if (task === "") {
      return;
    }

    axios.post('http://localhost/todos', { task })
      .then(res => {
        console.log(res.data);
        setInputValue("");
        fetchTodos()
      })

  };

  const deleteTodo = (id) => {
    axios.delete(`http://localhost/todos/${id}`)
      .then(res => {
        console.log("ss");
        fetchTodos()
      })
  };

  const editTodo = (id, updateTask) => {

    axios.put(`http://localhost/todos/${id}`, { task: updateTask })
      .then(
        fetchTodos()
      )
  }

  return (
    <Row justify="center" >
      <Col span={6} style={{ border: "1px solid salmon" }}>
        <Row>
          <Col span={20}>
            <Input value={inputValue} onChange={(e) => setInputValue(e.target.value)} />
          </Col>
          <Col span={4}>
            <Button onClick={addTodo} style={{ width: "100%" }}>Add</Button>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <List
              size="small"
              header={<div>Todo List</div>}
              bordered
              dataSource={todoList}
              loading={loading}
              renderItem={item => (
                <>
                  <TodoItem item={item} deleteTodo={deleteTodo} editTodo={editTodo} />
                </>
              )}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default App;