import { useState } from 'react'
import { Col, Row, Input, Button, Divider, Radio } from "antd";
import Header from './components/Header'
import SubHeader from './components/SubHeader';
import CartHeader from './components/CartHeader';
import ItemList from './components/ItemList';
import PaymentBox from './components/PaymentBox'
const { TextArea } = Input;

function App() {
  const [paymentType, setPaymentType] = useState(1)
  const [itemsInCart, setItemsInCart] = useState(
    [
      {
        name: "Concrete Planter Large | Concrete Pot | Organiser | Office Stationery | Flower Pot | Cactus Plant | Concrete Storage",
        seller: "byJesse",
        profile_url: "https://upload.wikimedia.org/wikipedia/commons/f/fb/Wikisource-logo.png",
        product_url: "https://c.ndtvimg.com/2018-09/d89bk5cg_iphone-xs,-iphone-xs-max_640x480_13_September_18.jpg",
        price: 900.25,
        quantity: 1
      },
      {
        name: "Samsung Galaxy S20 | Samsung SmartPhone | Samsung Phone",
        seller: "Samsung",
        profile_url: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Samsung_logo_blue.png/1200px-Samsung_logo_blue.png",
        product_url: "https://www.flashfly.net/wp/wp-content/uploads/2020/03/Galaxy-S20-GPS.jpg",
        price: 600.15,
        quantity: 1
      }
    ])

  const onChange = e => {
    setPaymentType(e.target.value)
  }

  const onChangeQuantity = (value, index) => {
    const newItemsInCart = [...itemsInCart]
    newItemsInCart[index].quantity = value
    setItemsInCart(newItemsInCart)
  }

  const onRemove = (index) => {
    console.log(index);
    const newItemsInCart = itemsInCart.filter((item, idx) => { return index !== idx })
    setItemsInCart(newItemsInCart)
  }

  return (
    <div>
      <Header />
      <SubHeader />
      <Row>
        <Col span={24} style={{ backgroundColor: "rgba(238, 238, 238, 1)" }}>
          <CartHeader numItem={itemsInCart.length} />
          <Row justify="center">
            <Col span={15} style={{ padding: "10px 10px", backgroundColor: "white", border: "1px solid hsl(0, 0%, 90%)" }}>
              <Row>
                <ItemList items={itemsInCart} onRemove={onRemove} onChangeQuantity={onChangeQuantity} />
                <PaymentBox items={itemsInCart} onChange={onChange} paymentType={paymentType} />
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}

export default App;