import React from 'react'
import { Row, Col, Button } from 'antd'

function CartHeader(props) {
    return (
        <Row justify="center" style={{ padding: "20px 50px" }}>
            <Col span={8}
                style={{
                    display: "flex",
                    alignItems: "center",
                    fontSize: "24px",
                    fontWeight: "500"
                }}>
                <div>{props.numItem} item in your cart</div>
            </Col>
            <Col span={8}
                style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    alignItems: "center",
                }}>
                <Button type="primary">Keep shopping</Button>
            </Col>
        </Row>
    )
}

export default CartHeader
