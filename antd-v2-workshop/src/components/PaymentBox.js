import React from 'react'
import { Col, Radio, Divider, Button } from 'antd'

function PaymentBox(props) {
    return (
        <Col span={8} style={{ padding: "10px 10px", display: "flex", flexDirection: "column" }}>
            <div>How you'll pay</div>
            <Radio.Group onChange={props.onChange} value={props.paymentType}>
                <Radio style={{
                    display: 'block',
                    height: '30px',
                    lineHeight: '30px',
                }} value={1}>
                    <img src="https://news.siamphone.com/upload/news/nw14299/featureimg.jpg" style={{ border: "1px solid darkgray", width: "25px", height: "18px", marginRight: "2px" }} />
                    <img src="https://financialit.net/sites/default/files/mastercard.png" style={{ border: "1px solid darkgray", width: "25px", height: "18px", marginRight: "2px" }} />
                    <img src="https://www.paymentmedia.com/gallery/5a8d71dd9ccc6Font-Amex-Logo_623.jpg" style={{ border: "1px solid darkgray", width: "25px", height: "18px", marginRight: "2px" }} />
                    <img src="https://cdn2.downdetector.com/static/uploads/logo/Discover-logo.jpg" style={{ border: "1px solid darkgray", width: "25px", height: "18px", marginRight: "2px" }} />
                </Radio>
                <Radio style={{
                    display: 'block',
                    height: '30px',
                    lineHeight: '30px',
                }} value={2}>
                    <img src="https://i0.wp.com/www.whatphone.net/wp-content/uploads/2015/03/paypal.jpg?fit=690%2C388&ssl=1" style={{ border: "1px solid darkgray", width: "25px", height: "18px", marginRight: "2px" }} />
                </Radio>
            </Radio.Group>
            <div style={{ display: "flex", justifyContent: "space-between", marginTop: "20px" }}>
                <div>Item(s) total</div>
                <div style={{ textAlign: "end" }}>${props.items.reduce((acc, item) => { return acc += (item.price * item.quantity) }, 0)}</div>
            </div>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>Shipping</div>
                <div style={{ textAlign: "end" }}>$10.00</div>
            </div>
            <div style={{ display: "flex" }}>
                <div>To {<a>France</a>}</div>
            </div>
            <Divider />
            <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>Total</div>
                <div style={{ textAlign: "end" }}>${props.items.reduce((acc, item) => { return acc += (item.price * item.quantity) }, 0) + 10.00}</div>
            </div>
            <Button type="primary" style={{ background: "green" }} size="middle">Process to checkout</Button>
            <div style={{ fontSize: "10px", fontWeight: "300" }}>VAT included (where applicable). Additional duties and taxes may apply</div>
        </Col>
    )
}

export default PaymentBox
