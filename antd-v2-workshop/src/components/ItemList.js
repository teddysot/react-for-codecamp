import React from 'react'
import { Row, Col, Image, InputNumber, Input } from 'antd'
const { TextArea } = Input;

function ItemList(props) {
    return (
        <Col span={16} style={{ padding: "10px 10px", borderRight: "1px solid lightgray" }}>
            {props.items.map((item, index) => {
                return (
                    <>
                        <Row>
                            <Col span={16} style={{ display: "flex", padding: "20px 10px" }}>
                                <Image src={item.profile_url} width={25} />
                                <span style={{ marginLeft: "10px", fontWeight: "600" }}>{item.seller}</span>
                            </Col>
                            <Col span={8} style={{ display: "flex", justifyContent: "flex-end", padding: "20px 0" }}>
                                <span>Contact shop</span>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col span={6} style={{ marginLeft: "10px" }}>
                                <Image src={item.product_url} />
                            </Col>
                            <Col span={8}>
                                <div style={{ fontSize: "14px", fontWeight: "500" }}>{item.name}</div>
                                <div style={{ padding: "10px 0" }}>
                                    <div style={{ display: "flex" }}>
                                        <a >Save for later</a>
                                        <a onClick={() => props.onRemove(index)} style={{ marginLeft: "10px" }}>Remove</a>
                                    </div>
                                </div>
                            </Col>
                            <Col span={4}>
                                <InputNumber min={1} max={10} defaultValue={1} onChange={(value) => props.onChangeQuantity(value, index)} style={{ width: "60px" }} />
                            </Col>
                            <Col span={5} style={{ display: "flex", justifyContent: "flex-end" }}>
                                <span style={{ color: "green", fontWeight: "600" }}>${item.price * item.quantity}</span>
                            </Col>
                        </Row>
                        <Row justify="space-between" style={{ padding: "20px 0" }}>
                            <Col span={11} style={{ marginLeft: "10px" }}>
                                <TextArea placeholder="Add an optional to seller" rows={2} />
                            </Col>
                            <Col span={10} >
                                <div style={{ color: "darkgray", textAlign: "end" }}>Ready to ship in 1-3 business days</div>
                                <div style={{ color: "darkgray", textAlign: "end" }}>from United Kingdom</div>
                            </Col>
                        </Row>
                    </>
                )
            })}
        </Col>
    )
}

export default ItemList
