import React from 'react'
import { Row, Col, Image, Input } from 'antd'
import { HomeOutlined } from "@ant-design/icons";
const { Search } = Input;
const mainMenus = [
    { name: "Home", icon: <HomeOutlined /> },
    { name: "Favorites", icon: <HomeOutlined /> },
    { name: "You", icon: <HomeOutlined /> },
    { name: "Cart", icon: <HomeOutlined /> },
];

function Header() {
    return (
        <Row
            justify="center"
            style={{
                borderBottom: "1px solid hsl(0, 0%, 90%)",
                padding: "20px 0"
            }}
        >
            <Col
                span="8"
                style={{
                    display: "flex",
                    alignItems: "center",
                }}
            >
                <Image src="https://i.imgur.com/HAWQjSt.png" alt="brand logo" width="200px" />
                <Search
                    placeholder="input search text"
                    allowClear
                    enterButton="Search"
                    size="large"
                    onSearch={() => { }}
                />
            </Col>
            <Col
                span="8"
                style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "flex-end",
                }}
            >
                <ul
                    style={{
                        display: "flex",
                    }}
                >
                    {mainMenus.map((menu, index) => {
                        const isLastElement = mainMenus.length - 1 === index;
                        return (
                            <li
                                style={{
                                    display: "flex",
                                    flexDirection: "column",
                                    listStyleType: "none",
                                    paddingRight: isLastElement ? 0 : 20,
                                    marginRight: isLastElement ? 0 : 20,
                                    borderRight: isLastElement
                                        ? ""
                                        : "1px solid hsl(0, 0%, 90%)",
                                }}
                            >
                                {menu.icon}
                                <span>{menu.name}</span>
                            </li>
                        );
                    })}
                </ul>
            </Col>
        </Row>
    )
}

export default Header
