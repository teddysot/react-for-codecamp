import React from 'react'
import { Row, Col } from 'antd'

const subMenus = [
    { name: "Menu1" },
    { name: "Menu2" },
    { name: "Menu3" },
    { name: "Menu3" },
    { name: "Menu3" },
    { name: "Menu3" },
    { name: "Menu3" },
    { name: "Menu3" },
    { name: "Menu3" },
    { name: "Menu3" },
    { name: "Menu3" },
];

function SubHeader() {
    return (
        <Row
            justify="center"
            style={{ borderBottom: "1px solid hsl(0, 0%, 90%)" }}
        >
            <Col span={16}>
                <ul
                    style={{
                        display: "flex",
                        listStyleType: "none",
                        padding: "10px 0",
                    }}
                >
                    {subMenus.map((menu, index) => {
                        const isFirstElement = index === 0;
                        return (
                            <li style={{ marginLeft: isFirstElement ? 0 : 20 }}>
                                {menu.name}
                            </li>
                        );
                    })}
                </ul>
            </Col>
        </Row>
    )
}

export default SubHeader
