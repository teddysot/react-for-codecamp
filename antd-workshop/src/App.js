import React from 'react';
import './App.css';
import { Redirect, Route, Switch } from 'react-router-dom';
import HomePage from './pages/HomePage';
import TodolistPage from './pages/TodolistPage';
import ResumePage from './pages/ResumePage';

function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/todolist" component={TodolistPage} />
        <Route path="/resume" component={ResumePage} />
        <Redirect to='/' />
      </Switch>
    </div>
  );
}

export default App;
