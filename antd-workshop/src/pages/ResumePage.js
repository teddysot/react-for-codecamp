import React, { Component } from 'react';
import HeaderComponent from "../components/Header";
import TimelineComponent from "../components/Timeline"

export default class ResumePage extends Component {
  state = {
    isWhite: true,
    inputValue: ""
  }

  changeIsWhite = () => {
    this.setState({
      isWhite: !this.state.isWhite
    });
  }

  changeInputValue = (event) => {
    this.setState({
      inputValue: event.target.value
    })
  }

  render() {
    const bgColor = this.state.inputValue;
    const timelineContainer = {
      display: "flex",
      alignItems: "center"
    }


    const timelineBody = {
      display: "flex",
      flexDirection: "column",
      width: "70vw",
      marginLeft: "5%",
      marginRight: "5%"
    }

    const timelineSubject = {
      fontWeight: "bold",
      color: "#0563bb",
      textDecorationLine: "underline"
    }

    const buttonChange = {
      fontSize: this.state.isWhite ? "18px" : "13px",
      width: this.state.isWhite ? "170px" : "100px",
      height: this.state.isWhite ? "50px" : "30px"
    }
    return (
      <div className="App" style={{ backgroundColor: bgColor }}>
        <input onChange={this.changeInputValue} />
        <button style={buttonChange} onClick={this.changeIsWhite}>Change Color</button>
        <HeaderComponent
          isWhite={this.state.isWhite}
          inputValue={this.state.inputValue}
          titleName="Saharat Nasahachart"
          location="Bangkok, Thailand"
          email="saharat.teddy@gmail.com"
          subject="Objective"
          detail="Work as a gameplay programmer on an online competitive multiplayer game, and
          continue to develop my programming skills, learning new experiences, and solving
    problems to achieve new creative content for the future of my career"/>
        <div style={timelineContainer}>
          <div style={timelineBody}>
            <h2 style={timelineSubject}>Work Experience</h2>
            <TimelineComponent
              isWhite={this.state.isWhite}
              subject="Titanserv.Cloud"
              location="(Bangkok, Thailand)"
              title="Founder (Part-time)"
              timeline="Sep 2019 - Present"
              li1="Selling and marketing products (VPS and Dedicated Server)"
              li2="Remoting and monitoring servers using XenServer and VMWare vSphere" />
            <TimelineComponent
              isWhite={this.state.isWhite}
              subject="Patsara Thai Cuisine"
              location="(Vancouver, Canada)"
              title="Kitchen Helper (Part-time)"
              timeline="Dec 2017 - Dec 2018"
              li1="Preparing materials and soup"
              li2="Organizing equipments" />
            <TimelineComponent
              isWhite={this.state.isWhite}
              subject="Sportchek"
              location="(Vancouver, Canada)"
              title="Stock/ Customer Service (Part-time)"
              timeline="May 2015 - Jun 2015"
              li1="Arranging items in stock"
              li2="Helping Customer" />
          </div>
          <div style={timelineBody}>
            <h2 style={timelineSubject}>Education</h2>
            <TimelineComponent
              isWhite={this.state.isWhite}
              subject="Software Park Thailand"
              location="(Bangkok, Thailand)"
              title="Certificate, Fullstack Developer"
              timeline="Sep 2020 - Dec 2020"
              li1=""
              li2="" />
            <TimelineComponent
              isWhite={this.state.isWhite}
              subject="Vancouver Film School"
              location="(Vancouver, Canada)"
              title="Associate's Degree, Programming for Web, Mobile and Games"
              timeline="Jan 2019 - Dec 2019"
              li1=""
              li2="" />
            <TimelineComponent
              isWhite={this.state.isWhite}
              subject="The Art Institute of Vancouver"
              location="(Vancouver, Canada)"
              title="Bachelor's Degree, Computer Science in Visual and Game Programming"
              timeline="Jan 2016 - Dec 2018"
              li1=""
              li2="" />
          </div>
        </div>
      </div>
    );
  }
}
