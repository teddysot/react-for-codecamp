import React, { Component } from 'react';

class Header extends Component {
    render() {
        const headerContainer = {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            marginTop: "5vh",
        }

        const headerObjective = {
            display: "flex",
            flexDirection: "column",
            width: "100vw",
            marginLeft: "10%"
        }

        const headerName = {
            color: "#0563bb",
            fontSize: "32px",
            fontWeight: "bold",
            backgroundColor: this.props.isWhite ? "white" : "lime"
        }

        const headerSubject = {
            color: "#0563bb",
            textDecorationLine: "underline",
            width: "80%",
            backgroundColor: this.props.isWhite ? "white" : "lime"
        }

        const objective = {
            display: "flex",
            flexDirection: "column",
            width: "50vw"
        }
        return (

            <div style={headerContainer}>
                <div style={headerName}>Current BG Color:{this.props.inputValue}</div>
                <div style={headerName}>{this.props.titleName}</div>
                <div>{this.props.location}</div>
                <div>{this.props.email}</div>
                <div style={headerObjective}>
                    <h2 style={headerSubject}>{this.props.subject}</h2>
                    <div style={objective}>{this.props.detail}</div>
                </div>
            </div >
        );
    }
}

export default Header;