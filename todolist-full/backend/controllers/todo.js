const db = require('../models')

exports.getAllTodos = async (req, res) => {
    const allTodos = await db.Todo.findAll({ where: { user_id: req.user.id }, attributes: ["id", "task"] })
    res.status(200).send(allTodos)
}

exports.getTodoById = async (req, res) => {
    const targetTodo = await db.Todo.findOne({ where: { id: req.params.id }})
    if (targetTodo && targetTodo.user_id === req.user.id) {
        res.status(200).send(targetTodo);
    }
    else {
        res.status(404).send({ message: "Todo not found" })
    }
}

exports.createTodo = async (req, res) => {
    const { task } = req.body
    const newTodo = await db.Todo.create({ task, user_id: req.user.id })

    res.status(201).send(newTodo)
}

exports.updateTodo = async (req, res) => {
    const targetTodo = await db.Todo.findOne({ where: { id: req.params.id } })

    if (targetTodo && targetTodo.user_id === req.user.id) {
        targetTodo.update({ task: req.body.task })
        res.status(200).send({ message: "Task Updated" })
    }
    else {
        res.status(404).send({ message: "Not found" })
    }
}

exports.deleteTodo = async (req, res) => {
    const targetTodo = await db.Todo.findOne({ where: { id: req.params.id } })

    if (targetTodo && targetTodo.user_id === req.user.id) {
        await targetTodo.destroy()
        res.status(200).send({ message: "Task Deleted" })
    }
    else {
        res.status(404).send({ message: "Not found" })
    }
}