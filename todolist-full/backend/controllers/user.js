const db = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

exports.register = async (req, res) => {
    const { username, password, name } = req.body
    const targetUser = await db.User.findOne({ where: { username } })

    if (targetUser) {
        res.status(400).send({ message: "Username already taken" })
    }
    else {
        bcrypt.genSalt(Number(process.env.SALT_ROUND), (err, salt) => {
            if (err) {
                res.status(400).send({ message: "Signup Failed" });
            }

            bcrypt.hash(password, salt, async (err, hash) => {
                if (err) {
                    res.status(400).send({ message: "Signup Failed" });
                }

                await db.User.create({
                    username,
                    name,
                    password: hash
                })

                res.status(201).send({ message: "Signup Successfully" })
            })
        })
    }
}

exports.login = async (req, res) => {
    const { username, password } = req.body
    const targetUser = await db.User.findOne({ where: { username } })

    if (!targetUser) {
        res.status(400).send({ message: "Username not found" })
    }
    else {
        bcrypt.compare(password, targetUser.password, (err, isCorrect) => {
            if (err) {
                res.status(400).send({ message: "Authentication Failed" })
            }
            if (isCorrect) {
                const payload = {
                    id: targetUser.id,
                }

                const token = jwt.sign(payload, process.env.SECRET, { expiresIn: 3600 })
                res.status(200).send({ token })
            } else {
                res.status(400).send({ message: "Authentication Failed" })
            }
        });
    }
}