require('dotenv').config();
const express = require('express')
const cors = require('cors')
const app = express();
const db = require('./models')

const todoRoutes = require('./routes/todo')
const userRoutes = require('./routes/user')

require('./config/passport')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors())
app.use("/auth", userRoutes);
app.use("/todos", todoRoutes);


app.listen(process.env.PORT, () => {
    console.log("Server is running at port " + process.env.PORT);
})

db.sequelize.sync()
    .then(() => {
        console.log("Database established");
    })
    .catch(err => {
        console.log(err);
    })