import React from 'react'
import { Empty } from 'antd'

function NotFound() {
    return (
        <Empty description="Not Found" />
    )
}

export default NotFound
