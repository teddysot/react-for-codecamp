import { Avatar, Col, Dropdown, Menu, Row } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

function NavBar() {
    const menu = (
        <Menu>
            <Menu.Item>
                <Link to="#">ดูรายชื่อเพื่อน</Link>
            </Menu.Item>
            <Menu.Item>
                <Link to="#">เปลี่ยนรหัสผ่าน</Link>
            </Menu.Item>
            <Menu.Item>
                <Link to="#">ออกจากระบบ</Link>
            </Menu.Item>
        </Menu>
    );

    const ownerName = "Nuttachai Kulthammanit";
    const ownerPicture =
        "https://scontent.fbkk22-4.fna.fbcdn.net/v/t1.0-1/cp0/c0.0.40.40a/p40x40/71561886_1609849782479256_2419419055669641216_n.jpg?_nc_cat=111&_nc_sid=dbb9e7&_nc_eui2=AeEYlVBHsRFEbq4NZ0oBnzCivfkHy8Or-D69-QfLw6v4Pv5jxDuZ6xkmQTm2LlLC0NbT_Lul-opFcW7eFar9xQMg&_nc_ohc=8E3UiSB564IAX8jEPyr&_nc_ht=scontent.fbkk22-4.fna&oh=e4b79feeb1c3f76e02bd4d3156bd1030&oe=5EB806FA"

    return (
        <div style={{ height: "42px", backgroundColor: "#4267b2" }}>
            <Row>
                <Col xs={0} sm={0} md={0} lg={2} xl={2} xxl={2}></Col>
                <Col xs={4} sm={6} md={6} lg={6} xl={6} xxl={6}>
                    <Link to="/">
                        <img
                            alt="small-fakebook"
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAXVBMVEU6VZ////81Up0vTZwyT5xgc61+jbuXo8hFXaOirM0nSJmlrs7O1OUkRpkfQ5f29/q3v9iHlcDV2umvt9RoerJxgrbd4Ozp6/PHzeHByN5YbatQZ6h2hrhMY6aQnMSGvE5JAAAC3UlEQVR4nO3cWZKjMBBFUVrIEzZ4nqt6/8usqP5vLJDlfEncuwCCE0ZmkKCqiIiIiIiIiIiIiIiIiIjUCyHG+kXReidHF+u2q2Zf+8Wqt/XeJ7Fuw/l4+ZPSobHe2eHFJi4OSbrfNu6EsT3fknkOhbGbX4f4vAlDdx7ocyasq0HHpz9hNx/u8yQMzX0M0I8wLgePQF/C+rEdB/QirE8jfV6EcTzQhzA+xwNdCEM1dgx6EXZpNxF+he0uB+hAGL+ygA6Ebc4g9CDMPEb1heGRCZQXdumPK3wKwywXqC5sNxMXhpzLNRfC5jh1Ye65UF4Yz/lAbWGTe7aXF7YjH824EYblG4DSwrh/h1B57qkeMgy3h93a3fxhk35Nejs1TeNvDrhLPRten22w3tlRxUTgrvPpq8J3GvDeWe/p2BLvnC5uganCpdNDtEoVHoVPd69KEzr+CdOEl9Z6NzNKEh5r693MKEmofE32siThHKFyCBHqhxChfggR6ocQoX4IEeqHEKF+CBHqNw1h6HlFuUlZWvrVvnrT2Rh4WvS0ThDu+jbw28pWWKcgMrOdffuAcGs7RfwBofH02weE98kfpWvbP9MPCPeTF55s5/k/IPyeunBrfE1TXng1XjFVXrgxXo1SXrgzXjJVXriY/Di0vn8sL3wYL3srL7R+BFBcuLVem1lceLVeulhceJv8b2i+OLO48O/k/2nOkxc+rVeBFxdaD8PiQut7p/JC+3cPSwuNHyV+QLia/Di0f1ehtHBmfbIoLrT2FRdaP0osLzS/dyouFPjYQP2Gr7P0tLM/Sqtq2VPK5yDnfVuwxv0r/L+YtNqk7tmCNe5V01hP0xdChPohRKgfQoT6IUSoH0KE+iFEqB9ChPohRKgfQoT6IUSoH0KE+iFEqB9ChPohRKgfQoT6IUSoH0KE+iFEqB9ChPohRKgfQoT6IUSoH0KE+iFEqB9ChPohRKgfQoT6IUQ4oB9urELQFCwKGgAAAABJRU5ErkJggg=="
                            style={{ height: "42px" }}
                        />
                    </Link>
                </Col>
                <Col xs={12} sm={12} md={12} lg={10} xl={10} xxl={10}></Col>
                <Col xs={8} sm={6} md={6} lg={6} xl={6} xxl={6}>
                    <Row
                        style={{ height: "100%", color: "white" }}
                        justify="center"
                        align="middle"
                    >
                        <Col xs={4} sm={4} md={4} lg={4} xl={2} xxl={3}>
                            <Avatar size={24} src={ownerPicture} />
                        </Col>
                        <Col xs={0} sm={1} md={1} lg={1} xl={1} xxl={0}></Col>
                        <Col xs={14} sm={16} md={19} lg={19} xl={17} xxl={17}>
                            <Row>
                                <Dropdown overlay={menu} placement="bottomLeft">
                                    <strong style={{ fontSize: "12px" }}>
                                        <Link style={{ color: "white" }} to="#">
                                            {ownerName}
                                        </Link>
                                    </strong>
                                </Dropdown>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col xs={0} sm={0} md={0} lg={4}></Col>
            </Row>
        </div>
    );
}

export default NavBar;