import './App.css';
import React, { useState } from 'react'
import Counter from './containers/Counter/Counter'
import ResultList from './containers/ResultList/ResultList';
import { connect } from 'react-redux'
import { addSpecialResult } from './store/action';

function App(props) {
  const [showCounter, setShowCounter] = useState(true)
  const [showResultList, setShowResultList] = useState(true)

  return (
    <div className="App">
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <button className="button-codecamp" onClick={() => setShowCounter(!showCounter)}>{showCounter ? "Hide" : "Show"} Counter</button>
        <button className="button-codecamp" onClick={() => setShowResultList(!showResultList)}>{showResultList ? "Hide" : "Show"} Result List</button>
        <button className="button-codecamp" onClick={() => props.onAddSpecial()}>Add Special Result</button>
      </div>
      <Counter showCounter={showCounter} />
      <ResultList showResultList={showResultList} />
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAddSpecial: () => dispatch(addSpecialResult())
  }
}

export default connect(null, mapDispatchToProps)(App);
