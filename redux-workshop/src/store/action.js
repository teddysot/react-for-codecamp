import axios from 'axios'

export const INCREMENT = "INCREMENT"
export const DECREMENT = "DECREMENT"
export const ADD = "ADD"
export const SUBTRACT = "SUBTRACT"
export const STORE_RESULT = "STORE_RESULT"
export const DELETE_RESULT = "DELETE_RESULT"
export const ADD_SPECIAL_RESULT = "ADD_SPECIAL_RESULT"

// ACTION CREATORS
export const increment = () => {
    return { type: INCREMENT }
}

export const decrement = () => {
    return { type: DECREMENT }
}

export const add = (value) => {
    return { type: ADD, value }
}

export const subtract = (value) => {
    return { type: SUBTRACT, value }
}

export const storeResult = (value) => {
    return { type: STORE_RESULT, counter: value }
}

export const deleteResult = (id) => {
    return { type: DELETE_RESULT, id }
}

export const addSpecialResult = () => {
    return (dispatch) => {
        axios.get('https://run.mocky.io/v3/c4ef42da-f959-4b00-a0f3-00c11d853c86')
            .then(res => {
                dispatch(addSpecialResultActionCreator(res.data))
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export const addSpecialResultActionCreator = (specialList) => {
    return { type: ADD_SPECIAL_RESULT, spcRL: specialList }
}