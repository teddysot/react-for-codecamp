import React, { useEffect } from "react";
import { useInputForm, useInputFormNumber } from '../CustomHook/hook'
import "./index.css";

export default function SignupForm(props) {
  const FirstName = useInputForm();
  const LastName = useInputForm();
  const Width = useInputFormNumber();

  // componentDidMount
  useEffect(() => {
    Width.handleSetValueChange(window.innerWidth);
    return () => {
    }
  }, [])

  // componentDidUpdate
  useEffect(() => {
    window.addEventListener("resize", () => Width.handleSetValueChange(window.innerWidth));
    return () => {
      window.removeEventListener("resize", () => Width.handleSetValueChange(window.innerWidth));
    }
  }, [Width])


  useEffect(() => {
    document.title = `${FirstName.value} ${LastName.value}`;
    return () => {
    }
  })

  const resetForm = () => {
    FirstName.handleReform();
    LastName.handleReform();
    Width.handleReform();
    Width.handleSetValueChange(window.innerWidth);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    alert(JSON.stringify({ FirstName: FirstName.value, LastName: LastName.value, Width: Width.value }));
    resetForm();
  }

  return (
    <form className="form-container" onSubmit={handleSubmit}>
      <div className="field-container">
        <label htmlFor="firstname">First Name</label>
        <input
          id="firstname"
          name="firstname"
          type="text"
          value={FirstName.value}
          onChange={FirstName.handleValueChange}
        />
      </div>
      <div className="field-container">
        <label htmlFor="lastname">Last Name</label>
        <input
          id="lastname"
          name="lastname"
          type="text"
          value={LastName.value}
          onChange={LastName.handleValueChange}
        />
      </div>

      <div className="field-container">
        <span>Window Width: {Width.value}</span>
      </div>
      <button className="button" type="submit">
        Submit
        </button>
    </form>
  )
}