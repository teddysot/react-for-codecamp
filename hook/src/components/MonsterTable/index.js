import React, { useState, useEffect } from "react";

export default function MonsterTable(props) {
  const { monsters } = props;
  return (
    <table style={{ width: "100%", border: "1px solid black" }}>
      <thead>
        <th>Name</th>
        <th>Element</th>
        <th>HP</th>
        <th>MP</th>
        <th>Rating</th>
      </thead>
      <tbody>
        {monsters.map((monster, index) => {
          return <TableRow key={monster.id} {...monster} />;
        })}
      </tbody>
    </table>
  );
}

function TableRow(props) {

  const [BGColor, setBGColor] = useState("hsl(120, 100%, 50%)")
  const { name, hp, mp, element, rating } = props;

  useEffect(() => {
    const timerId = setInterval(() => {
      const saturation = Math.floor(Math.random() * 100);
      setBGColor(`hsl(120, ${saturation}%, 50%)`)
    }, 100);
    return () => {
      clearInterval(timerId);
    }
  }, [BGColor])

  return (
    <tr style={{ backgroundColor: BGColor }}>
      <td>{name}</td>
      <td>{element}</td>
      <td>{hp}</td>
      <td>{mp}</td>
      <td>{rating}</td>
    </tr>
  );
}