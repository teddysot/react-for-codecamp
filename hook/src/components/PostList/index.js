import React, { useContext } from "react";
import { ThemeContext } from '../../pages/Exercise8/index'
import "./index.css";

function PostList(props) {
  return (
    <div className="post-list">
      {props.posts.map((post) => {
        return (
          <Post
            key={post.id}
            title={post.title}
            description={post.description}
            author={post.author}
          />
        );
      })}
    </div>
  );
}

function Post(props) {
  const { title, description, author } = props;
  const { theme } = useContext(ThemeContext)
  const actionThemeStyle = {
    backgroundColor: theme === 'light' ? 'var(--main-branding-color-200)' : 'lightgray'
  }
  const userThemeStyle = {
    backgroundColor: theme === 'light' ? 'var(--main-branding-color-200)' : 'lightgray'
  }
  return (
    <div style={actionThemeStyle} className="post">
      <h1 style={userThemeStyle} className="post-title">{title}</h1>
      <p style={userThemeStyle} className="post-description">{description}</p>
      <p style={userThemeStyle} className="post-author">Author: {author}</p>
      <ActionMenu />
    </div>
  );
}

function ActionMenu() {
  const { theme, name } = useContext(ThemeContext)
  const actionThemeStyle = {
    backgroundColor: theme === 'light' ? 'var(--main-branding-color-200)' : 'lightgray'
  }
  const buttonThemeStyle = {
    backgroundColor: theme === 'light' ? 'var(--main-branding-color-500)' : 'darkgray'
  }
  return (
    <div style={actionThemeStyle} className="post-actions">
      <span style={actionThemeStyle} className="post-user">You are {name}</span>
      <button style={buttonThemeStyle} className="button post-edit-action-button">Edit</button>
      <button style={buttonThemeStyle} className="button post-delete-action-button">Delete</button>
    </div>
  );
}

export default PostList;
