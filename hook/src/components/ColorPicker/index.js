import React, { useState } from "react";
import "./index.css";

export default function ColorPicker() {
  const [color, setColor] = useState("")

  const handleColorChange = (e) => {
    setColor(e.target.value)
  }

  return (
    <div className="color-picker-container">
      <h2
        className="color-picker-heading"
        style={{ color: color }}
      >
        COLORFUL
        </h2>
      <input type="color" onChange={handleColorChange} />
    </div>
  )
}
