import React, { useState, useEffect } from "react";

export default function Clock() {
  const [date, setDate] = useState(new Date())

  const tick = () => {
    setDate(new Date())
  }
  const timerId = setInterval(() => {
    tick();
  }, 1000);
  
  useEffect(() => {
    return () => {
      clearInterval(timerId);
    }
  }, [date])

  return (
    <h1
      style={{
        textAlign: "center",
        fontSize: "60px",
        color: "var(--main-branding-color-500)",
      }}
    >
      {date.toLocaleTimeString()}
    </h1>
  )
}