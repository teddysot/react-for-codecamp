import React, { useState, useEffect } from 'react'
import "./index.css";

export default function HookCounter() {

    // Array destructuring
    const [counter, setCounter] = useState(0)

    const addCounter = () => {
        setCounter(counter + 1)
    }

    const subtractCounter = () => {
        setCounter(counter - 1)
    }

    const resetCounter = () => {
        setCounter(0)
    }

    // componentDidMount
    useEffect(() => {

        return () => {
        }
    }, [])

    // componentDidUpdate
    useEffect(() => {

        return () => {
            /**
             * Cleanup function
             * Cancel network function
             * clearInterval(timerId)
             * componentWillUnmount
             */
        }
    }, [counter])

    return (
        <div className="counter-app">
            <h2 className="counter-value">{counter}</h2>
            <div className="counter-menu">
                <button
                    className="button counter-add-button"
                    onClick={addCounter}
                >
                    +
          </button>
                <button
                    className="button counter-subtract-button"
                    onClick={subtractCounter}
                >
                    -
          </button>
                <button
                    className="button counter-reset-button"
                    onClick={resetCounter}
                >
                    reset
          </button>
            </div>
        </div>
    )
}
