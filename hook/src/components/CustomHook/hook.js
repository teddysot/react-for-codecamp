import React, { useState } from 'react'

// Custom Hook
export function useInputForm() {
    const [value, setValue] = useState("");

    function handleValueChange(e) {
        setValue(e.target.value)
    }

    function handleReform() {
        setValue("")
    }

    return {
        value: value,
        handleValueChange: handleValueChange,
        handleReform: handleReform,
    }
}

export function useInputFormNumber() {
    const [value, setValue] = useState(0);

    function handleSetValueChange(value) {
        setValue(value)
    }

    function handleValueChange(e) {
        setValue(e.target.value)
    }

    function handleReform() {
        setValue(0)
    }

    return {
        value: value,
        handleValueChange: handleValueChange,
        handleSetValueChange: handleSetValueChange,
        handleReform: handleReform,
    }
}