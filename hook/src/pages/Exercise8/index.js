import React, { createContext, useState } from "react";
import Instruction from "../../components/Instruction";
import PostList from "../../components/PostList";
import mockedPosts from "../../mocks/posts";

export const ThemeContext = createContext();

function Exercise8() {
  const [theme, setTheme] = useState('light')

  const handleTheme = () => {
    switch (theme) {
      case 'light':
        setTheme('dark')
        break;
      case 'dark':
        setTheme('light')
        break;
      default:
        break;
    }
  }

  return (
    <div className="page-container">
      <button style={{ width: "100px", height: "50px", marginLeft: "50%" }} onClick={handleTheme}>Change Theme</button>
      <Instruction
        topic="Posts App Theme"
        description="Change theme of posts app with Context API"
      />
      <ThemeContext.Provider value={{ theme: theme, name: 'john' }}>
        <PostList posts={mockedPosts} />
      </ThemeContext.Provider>
    </div>
  );
}

export default Exercise8;
