import React, { useState } from "react";
import Instruction from "../../components/Instruction";
import PostList from "../../components/PostList";
import mockedPosts from "../../mocks/posts";
import { ThemeContext } from '../../pages/Exercise8/index'

function Exercise9() {
  const [theme, setTheme] = useState('light')

  const handleTheme = () => {
    switch (theme) {
      case 'light':
        setTheme('dark')
        break;
      case 'dark':
        setTheme('light')
        break;
      default:
        break;
    }
  }
  return (
    <div className="page-container">
      <Instruction
        topic="Posts App (useState)"
        description="Make Post App be able to Add / Edit / Delete with useState"
      />
      <ThemeContext.Provider value={{ theme: theme, name: 'john' }}>
        <PostList posts={mockedPosts} />
      </ThemeContext.Provider>
    </div>
  );
}

export default Exercise9;
