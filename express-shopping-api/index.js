const express = require('express');
const productRouter = require('./routes/productRoute')
const authRouter = require('./routes/authRoute')

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }))
app.use(express.static('./public/'))
app.use(express.static('./uploads/'))

app.use('/product', productRouter)
app.use('/auth', authRouter)

app.use((req, res, next) => {
    next(new Error('Path not found'))
})

app.use((err, req, res, next) => {
    res.status(404).send(err.message)
})

const port = 80;
app.listen(port, () => {
    console.log(`server starting on port ${port}`);
});