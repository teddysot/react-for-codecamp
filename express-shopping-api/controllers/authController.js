const fs = require('fs');

const filePath = './mocks/user.json'

const userLists = JSON.parse(fs.readFileSync(filePath));

exports.getUsers = (req, res) => {
    const searchTask = req.query.search;

    if (searchTask) {
        const filteredList = userLists.filter((e) => e.username.includes(searchTask))
        return res.status(200).send(filteredList)
    }

    res.status(200).send(userLists);
};

exports.validateId = (req, res, next) => {
    const id = +req.params.id;
    const idx = userLists.findIndex(el => el.id === id);

    if (idx === -1) {
        next(new Error('Invalid Id'))
    }

    req.idx = idx
    req.id = id
    next();
}

exports.getUser = (req, res) => {
    const user = userLists.filter(el => el.id === req.id);
    res.status(200).send(user[0]);
};

exports.createUser = (req, res) => {
    const newId = userLists.length > 0 ? userLists[userLists.length - 1].id + 1 : 0;
    const newUserList =
    {
        id: newId,
        username: req.body.username,
        password: req.body.password,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        role: req.body.role,
        lastLogin: ""
    };

    userLists.push(newUserList);

    fs.writeFileSync(filePath, JSON.stringify(userLists));
    res.status(201).send(newUserList);
};

const requestValidation = (req) => {
    let newPassword = userLists[req.idx].password,
        newFirstName = userLists[req.idx].firstname,
        newLastName = userLists[req.idx].lastname,
        newRole = userLists[req.idx].role;

    if (req.body.password !== "" && req.body.password !== null && req.body.password !== undefined) {
        newPassword = req.body.password
    }

    if (req.body.firstname !== "" && req.body.firstname !== null && req.body.firstname !== undefined) {
        newFirstName = req.body.firstname
    }

    if (req.body.lastname !== "" && req.body.lastname !== null && req.body.lastname !== undefined) {
        newLastName = req.body.lastname
    }

    if (req.body.role !== "" && req.body.role !== null && req.body.role !== undefined) {
        newRole = req.body.role
    }

    return {
        password: newPassword,
        firstname: newFirstName,
        lastname: newLastName,
        role: newRole
    }
}

exports.updateUser = (req, res) => {

    const newUpdateUser = requestValidation(req);

    userLists[req.idx] =
    {
        id: req.id,
        username: userLists[req.idx].username,
        password: newUpdateUser.password,
        firstname: newUpdateUser.firstname,
        lastname: newUpdateUser.lastname,
        role: newUpdateUser.role,
        lastLogin: userLists[req.idx].lastLogin
    };

    fs.writeFileSync(filePath, JSON.stringify(userLists));
    res.status(200).send(userLists);
};

exports.deleteUser = (req, res) => {
    const newUserLists = userLists.filter(el => el.id !== req.id);
    fs.writeFileSync(filePath, JSON.stringify(newUserLists));
    res.status(204).send();
};