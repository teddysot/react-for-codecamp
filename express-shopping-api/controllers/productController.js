const fs = require('fs');

const filePath = './mocks/product.json'

const productLists = JSON.parse(fs.readFileSync(filePath));

exports.getProducts = (req, res) => {
    const searchTask = req.query.search;

    if (searchTask) {
        const filteredList = productLists.filter((e) => e.name.includes(searchTask))
        return res.status(200).send(filteredList)
    }

    res.status(200).send(productLists);
};

exports.validateId = (req, res, next) => {
    const id = +req.params.id;
    const idx = productLists.findIndex(el => el.id === id);

    if (idx === -1) {
        next(new Error('Invalid Id'))
    }

    req.idx = idx
    req.id = id
    next();
}

exports.getProduct = (req, res) => {
    const product = productLists.filter(el => el.id === req.id);
    res.status(200).send(product[0]);
};

exports.createProduct = (req, res) => {
    const newId = productLists.length > 0 ? productLists[productLists.length - 1].id + 1 : 0;
    const newProductList =
    {
        id: newId,
        name: req.body.name,
        price: +req.body.price,
        category: req.body.category,
        image: [],
        totalSale: +req.body.totalSale
    };

    productLists.push(newProductList);

    fs.writeFileSync(filePath, JSON.stringify(productLists));
    res.status(201).send(newProductList);
};

const requestValidation = (req) => {
    let newName = productLists[req.idx].name,
        newPrice = productLists[req.idx].price,
        newCategory = productLists[req.idx].category,
        newImage = productLists[req.idx].image,
        newTotalSale = productLists[req.idx].totalSale

    if (req.body.name !== "" && req.body.name !== null && req.body.name !== undefined) {
        newName = req.body.name
    }

    if (req.body.price !== "" && req.body.price !== null && req.body.price !== undefined) {
        newPrice = +req.body.price
    }

    if (req.body.category !== "" && req.body.category !== null && req.body.category !== undefined) {
        newCategory = req.body.category
    }

    if (req.files) {
        for (let i = 0; i < req.files.length; i++) {
            newImage.push(req.files[i].path)
        }
    }

    if (req.body.totalSale !== "" && req.body.totalSale !== null && req.body.totalSale !== undefined) {
        newTotalSale = +req.body.totalSale
    }

    return {
        name: newName,
        price: newPrice,
        category: newCategory,
        image: newImage,
        totalSale: newTotalSale
    }
}

exports.uploadImage = (req, res) => {

    const files = req.files
    if (!files) {
        const error = new Error('Please choose files')
        error.httpStatusCode = 400
        return next(error)
    }

    res.send(files)
}

exports.updateProduct = (req, res) => {

    const newUpdateProduct = requestValidation(req);

    productLists[req.idx] =
    {
        id: req.id,
        name: newUpdateProduct.name,
        price: newUpdateProduct.price,
        category: newUpdateProduct.category,
        image: newUpdateProduct.image,
        totalSale: newUpdateProduct.totalSale
    };

    fs.writeFileSync(filePath, JSON.stringify(productLists));
    res.status(200).send(productLists);
};

exports.deleteProduct = (req, res) => {
    const newProductLists = productLists.filter(el => el.id !== req.id);
    fs.writeFileSync(filePath, JSON.stringify(newProductLists));
    res.status(204).send();
};