const express = require('express')
const authController = require('../controllers/authController')

const router = express.Router();

router.get('/', authController.getUsers);
router.post('/', authController.createUser);
router.get('/:id', authController.validateId, authController.getUser);
router.patch('/:id', authController.validateId, authController.updateUser);
router.delete('/:id', authController.validateId, authController.deleteUser);

module.exports = router;