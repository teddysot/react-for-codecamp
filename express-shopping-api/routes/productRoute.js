const express = require('express')
const multer = require('multer')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.' + file.mimetype.split('/')[1])
    }
})
const upload = multer({ storage: storage })
const productController = require('../controllers/productController')

const router = express.Router();

router.get('/', productController.getProducts);
router.post('/', productController.createProduct);
router.post('/photos/upload', upload.array('photos', 12), productController.uploadImage)

router.get('/:id', productController.validateId, productController.getProduct);
router.patch('/:id', productController.validateId, upload.array('photos', 12), productController.updateProduct);
router.delete('/:id', productController.validateId, productController.deleteProduct);

module.exports = router;