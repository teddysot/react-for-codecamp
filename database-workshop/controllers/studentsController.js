const { uniqueId } = require('lodash')
const students = [];

exports.getStudents = (req, res) => {
    res.status(200).send(students)
}

exports.getStudentsFromId = (req, res) => {

    const targetId = req.params.id;

    const targetStudent = students.find(std => std.id === targetId)

    res.status(200).send(targetStudent)
}

exports.addStudent = (req, res) => {
    const name = req.body.name;
    const age = req.body.age;
    const gender = req.body.gender;

    const newStudent = { id: uniqueId(), name, age, gender }

    students.push(newStudent)

    res.status(201).send(newStudent)
}

exports.updateStudent = (req, res) => {

    const targetId = req.params.id;
    const { name, age, gender } = req.body;
    const targetStudentIdx = students.findIndex(std => std.id === targetId)

    students[targetStudentIdx] = { id: targetId, name, age, gender }
    res.status(200).send({ message: "Updated" })
}

exports.deleteStudent = (req, res) => {
    const targetId = req.params.id;

    students = students.filter(std => std.id !== targetId)

    res.status(204).send()
}