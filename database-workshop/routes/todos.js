const express = require('express')
const todoController = require('../controllers/todosController')

const router = express.Router();

router.get('/', todoController.getTodos);
router.post('/', todoController.createTodo);
router.get('/:id', todoController.validateId, todoController.getTodo);
router.put('/:id', todoController.validateId, todoController.updateTodo);
router.delete('/:id', todoController.validateId, todoController.deleteTodo);

module.exports = router;