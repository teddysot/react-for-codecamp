const express = require('express')
const studentsController = require('../controllers/studentsController')
const router = express.Router();

router.get('/', studentsController.getStudents)
router.get('/:id', studentsController.getStudentsFromId)
router.post('/', studentsController.addStudent)
router.put('/:id', studentsController.updateStudent)
router.delete('/:id', studentsController.deleteStudent)

module.exports = router;