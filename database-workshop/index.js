const express = require('express')
const cors = require('cors')
const app = express();
const port = 80

const studentsRoute = require('./routes/students')
const todosRoute = require('./routes/todos')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/todos', todosRoute)
app.use('/students', studentsRoute)

app.listen(port, () => {
    console.log(`Server is starting at port: ${port}`);
})