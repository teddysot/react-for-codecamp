import React, { Component } from 'react';

class Timeline extends Component {
    render() {
        const timelineName = {
            fontWeight: "bold",
            color: "#0563bb",
            backgroundColor: this.props.isWhite ? "white" : "lime"
        }

        const timelineSubSubject = {
            fontStyle: "italic"
        }

        const timelineWork = {
            background: "#e6fffa",
            borderRadius: "5px",
            padding: "5px 10px",
            width: "12em",
            textAlign: "center"
        }

        return (
            <>
                <div style={timelineName}>{this.props.subject}</div>
                <div style={timelineSubSubject}>{this.props.location}</div>
                <div style={timelineSubSubject}>{this.props.title}</div>
                <div style={timelineWork}>{this.props.timeline}</div>
                <ul>
                    <li>{this.props.li1}</li>
                    <li>{this.props.li2}</li>
                </ul>
            </>
        );
    };
}

export default Timeline;