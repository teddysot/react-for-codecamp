import React, { Component } from 'react';
import moment from 'moment';
import { Avatar, Breadcrumb, Button, Form, Layout, Menu, Typography, Switch, List, Modal, Input, DatePicker, Row, Col } from 'antd';
import './App.css';
import SubMenu from 'antd/lib/menu/SubMenu';
import { FolderOutlined, DeleteOutlined, RightSquareTwoTone, FolderAddOutlined, EditOutlined, CheckCircleOutlined } from '@ant-design/icons';
const { Header, Footer, Sider, Content, } = Layout;
const { Title } = Typography;

class App extends Component {
  state = {
    mode: 'inline',
    theme: 'dark',
    visible: false,
    editVisible: false,
    todoList: [],
    doingList: [],
    doneList: [],
  };

  data = []
  keyPrimaryCount = 0


  layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  showEditModal = (target) => {
    this.currentEditList = target
    this.setState({
      editVisible: true,
    });
  };

  removeList = (list) => {
    const targetStatus = list.status
    const newList = this.data.filter(e => e.id !== list.id);
    this.data = newList
    const newTargetList = this.data.filter(e => e.status === targetStatus);

    switch (targetStatus) {
      case "todo":
        this.setState({ todoList: newTargetList })
        break;
      case "doing":
        this.setState({ doingList: newTargetList })
        break;
      case "done":
        this.setState({ doneList: newTargetList })
        break;
      default:
        break;
    }
  }

  editList = (targetId, list) => {
    const targetIdx = this.data.findIndex(e => e.id === targetId)
    this.data[targetIdx] = list;
    const targetStatus = this.data[targetIdx].status
    const newTodoList = this.data.filter(e => e.status === targetStatus);
    switch (targetStatus) {
      case "todo":
        this.setState({ todoList: newTodoList })
        break;
      case "doing":
        this.setState({ doingList: newTodoList })
        break;
      case "done":
        this.setState({ doneList: newTodoList })
        break;
      default:
        break;
    }
  }

  addList = (tt, desc, date) => {
    this.data.push({ title: tt, description: desc, date: date, id: ++this.keyPrimaryCount, status: "todo" })
    const newTodoList = this.data.filter(e => e.status === 'todo')
    this.setState({
      todoList: newTodoList
    })
  }

  moveToDoing = () => {
    const targetIdx = this.data.findIndex(e => e.id === this.currentEditList.id)
    const targetStatus = this.currentEditList.status
    this.data[targetIdx].status = 'doing'
    const newList = this.data.filter(e => e.status === targetStatus);
    const newDoingList = this.data.filter(e => e.status === 'doing');
    this.setState({ doingList: newDoingList })

    switch (targetStatus) {
      case "todo":
        this.setState({ todoList: newList })
        break;
      case "done":
        this.setState({ doneList: newList })
        break;
      default:
        break;
    }

    this.closeModal()
  }

  moveToDone = () => {
    const targetIdx = this.data.findIndex(e => e.id === this.currentEditList.id)
    const targetStatus = this.currentEditList.status
    this.data[targetIdx].status = 'done'
    const newList = this.data.filter(e => e.status === targetStatus);
    const newDoneList = this.data.filter(e => e.status === 'done');
    this.setState({ doneList: newDoneList })

    console.log(targetStatus);

    switch (targetStatus) {
      case "todo":
        this.setState({ todoList: newList })
        break;
      case "doing":
        this.setState({ doingList: newList })
        break;
      default:
        break;
    }

    this.closeModal()
  }

  changeMode = value => {
    this.setState({ mode: value ? 'inline' : 'vertical' })
  }

  changeTheme = value => {
    this.setState({
      theme: value ? 'dark' : 'light',
    });
  };

  closeModal = () => {
    this.setState({
      visible: false,
      editVisible: false
    });
  };

  onFinish = values => {
    // console.log(moment(values.date).format('LL'));
    this.addList(values.title, values.desc, moment(values.date).format('LL'))
    this.closeModal()
  };

  onFinishEdit = values => {
    // console.log(moment(values.date).format('LL'));
    const newValues = { title: values.title, description: values.desc, date: moment(values.date).format('LL'), status: this.currentEditList.status }
    this.editList(this.currentEditList.id, newValues)
    this.closeModal()
  }

  onFinishFailed = errorInfo => {
    this.closeModal()
  };

  render() {
    return (
      <div className="App">
        <Modal
          title="Add Todo List"
          visible={this.state.visible}
          onCancel={this.closeModal}
          footer={[]}
        >
          <Form
            {...this.layout}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={this.onFinish}
            onFinishFailed={this.onFinishFailed}
          >
            <Form.Item
              label="Title"
              name="title"
              rules={[{ required: true, message: 'Please input todo list title' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Description"
              name="desc"
              rules={[{ required: true, message: 'Please input todo list description' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item label="DatePicker" name="date">
              <DatePicker />
            </Form.Item>
            <Form.Item {...this.tailLayout}>
              <Button type="primary" htmlType="submit">Submit</Button>
            </Form.Item>
          </Form>
        </Modal>
        <Modal
          title="Edit Todo List"
          visible={this.state.editVisible}
          onCancel={this.closeModal}
          footer={[]}
        >
          <Form
            {...this.layout}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={this.onFinishEdit}
            onFinishFailed={this.onFinishFailed}
          >
            <Form.Item
              label="Title"
              name="title"
              rules={[{ required: true, message: 'Please input todo list title' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Description"
              name="desc"
              rules={[{ required: true, message: 'Please input todo list description' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item label="DatePicker" name="date">
              <DatePicker />
            </Form.Item>
            <Form.Item {...this.tailLayout}>
              <Button type="primary" style={{ background: "orange", border: "1px solid orange", marginRight: "5px" }} onClick={() => { this.moveToDoing() }}>Doing</Button>
              <Button type="primary" style={{ background: "green", border: "1px solid green", marginRight: "5px" }} onClick={() => { this.moveToDone() }}>Done</Button>
            </Form.Item>
            <Form.Item {...this.tailLayout}>
              <Button type="primary" htmlType="submit">Edit</Button>
            </Form.Item>
          </Form>
        </Modal>
        <Layout>
          <Header style={{ padding: "15px" }}>
            <Avatar style={{ float: "right" }} src='./logo192.png' />
            <Switch
              style={{ float: "right", margin: "5px" }}
              checked={this.state.mode === 'inline'}
              onChange={this.changeMode}
              checkedChildren="Inline"
              unCheckedChildren="Vertical"
            />
            <Switch
              style={{ float: "right", margin: "5px" }}
              checked={this.state.theme === 'dark'}
              onChange={this.changeTheme}
              checkedChildren="Dark"
              unCheckedChildren="Light"
            />
            <Title style={{ float: "left", color: "white" }} level={3}>TODO LIST</Title>
          </Header>
          <Layout>
            <Sider>
              <Menu
                theme={this.state.theme}
                defaultSelectedKeys={['dashboard']}
                mode={this.state.mode}
              >
                <Menu.Item key='dashboard' style={{ fontWeight: "bold" }}>
                  Dashboard
                </Menu.Item>
                <SubMenu title={
                  <span>
                    <FolderOutlined />
                    <span style={{ fontWeight: "bold" }}>Projects</span>
                  </span>
                }>
                  <Menu.Item key='location1'>Project 1</Menu.Item>
                  <Menu.Item key='location2'>Project 2</Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Layout>
              <Content style={{ padding: '0 50px' }}>
                <Breadcrumb style={{ margin: '16px 0' }}>
                  <Breadcrumb.Item>Home</Breadcrumb.Item>
                  <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                  <Button style={{ float: 'right' }} type="primary" shape="primary" icon={<FolderAddOutlined />} size={'medium'} onClick={this.showModal}>Add Task</Button>
                </Breadcrumb>
                <Content style={{ backgroundColor: "#fff", padding: 24, minHeight: 580 }}>
                  <Row>
                    <Col span={8} style={{ padding: "5px" }}>
                      <h1 style={{ textAlign: "center", fontWeight: "bold", color: "blue" }}>TODO</h1>
                      <List
                        itemLayout="horizontal"
                        dataSource={this.state.todoList}
                        renderItem={item => (
                          <List.Item style={{ border: "1px solid blue", borderRadius: "10px", marginBottom: "2px" }}>
                            <List.Item.Meta
                              avatar={<Avatar icon={<RightSquareTwoTone />} />}
                              title={<a href="#">{item.title}</a>}
                              description={item.description}
                            />
                            <div>
                              <span style={{ marginRight: '4px' }}>{item.date}</span>
                              <Button style={{ float: 'right' }} icon={<DeleteOutlined />} size={'small'} onClick={() => { this.removeList(item) }} />
                              <Button style={{ float: 'right' }} icon={<EditOutlined />} size={'small'} onClick={() => { this.showEditModal(item) }} />
                            </div>
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col span={8} style={{ padding: "5px" }}>
                      <h1 style={{ textAlign: "center", fontWeight: "bold", color: "orange" }}>DOING</h1>
                      <List
                        itemLayout="horizontal"
                        dataSource={this.state.doingList}
                        renderItem={item => (
                          <List.Item style={{ border: "1px solid orange", borderRadius: "10px", marginBottom: "2px" }}>
                            <List.Item.Meta
                              avatar={<Avatar icon={<RightSquareTwoTone />} />}
                              title={<a href="#">{item.title}</a>}
                              description={item.description}
                            />
                            <div>
                              <span style={{ marginRight: '4px' }}>{item.date}</span>
                              <Button style={{ float: 'right' }} icon={<DeleteOutlined />} size={'small'} onClick={() => { this.removeList(item) }} />
                              <Button style={{ float: 'right' }} icon={<EditOutlined />} size={'small'} onClick={() => { this.showEditModal(item) }} />
                            </div>
                          </List.Item>
                        )}
                      />
                    </Col>
                    <Col span={8} style={{ padding: "5px" }}>
                      <h1 style={{ textAlign: "center", fontWeight: "bold", color: "green" }}>DONE</h1>
                      <List
                        itemLayout="horizontal"
                        dataSource={this.state.doneList}
                        renderItem={item => (
                          <List.Item style={{ border: "1px solid green", borderRadius: "10px", marginBottom: "2px" }}>
                            <List.Item.Meta
                              avatar={<Avatar icon={<RightSquareTwoTone />} />}
                              title={<a href="#">{item.title}</a>}
                              description={item.description}
                            />
                            <div>
                              <span style={{ marginRight: '4px' }}>{item.date}</span>
                              <Button style={{ float: 'right' }} icon={<DeleteOutlined />} size={'small'} onClick={() => { this.removeList(item) }} />
                              <Button style={{ float: 'right' }} icon={<EditOutlined />} size={'small'} onClick={() => { this.showEditModal(item) }} />
                            </div>
                          </List.Item>
                        )}
                      />
                    </Col>
                  </Row>
                </Content>
              </Content>
              <Footer style={{ textAlign: 'center' }}>Todo List Created by Saharat Nasahachart</Footer>
            </Layout>
          </Layout>
        </Layout>
      </div>
    );
  }
}

export default App;
