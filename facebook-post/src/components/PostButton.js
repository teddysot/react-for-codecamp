import React, { Component } from 'react';

class PostButton extends Component {


    render() {
        const postButtonContainer = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '15%',
            marginTop: '5px',
        }

        const postButtonBox = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#5f5f5f',
            borderRadius: '5px',
            width: '95%',
            height: '70%',
            border: '1px solid #4a4a4a',
            cursor: 'pointer'
        }

        const postButtonTitle = {
            color: 'whitesmoke',
            fontWeight: '500',
        }
        return (
            <div style={postButtonContainer}>
                <div style={postButtonBox}>
                    <div style={postButtonTitle}>Post</div>
                </div>
            </div>
        );
    }
}

export default PostButton;