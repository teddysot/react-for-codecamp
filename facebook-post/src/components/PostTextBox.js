import React, { Component } from 'react';

class PostTextBox extends Component {
    render() {
        const postTextContainer = {
            height: '20%'
        }
        return (
            <div style={postTextContainer}>
                <input className="post-text-input" type="text" placeholder="What's on your mind, Saharat" />
            </div>
        );
    }
}

export default PostTextBox;