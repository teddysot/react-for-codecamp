import React, { Component } from 'react';

class PostProfile extends Component {
    render() {
        const postProfile = {
            display: 'flex',
            height: '15%',
            marginLeft: '5%',
            marginTop: '5%'
        }

        const postProfileName = {
            display: 'flex',
            color: 'white',
            fontWeight: 'bold'
        }

        const postProfileContainer = {
            display: 'flex',
            flexDirection: 'column',
            marginLeft: '3%'
        }

        const postProfileImg = {
            height: '40px',
            width: '40px',
            borderRadius: '50%',
        }
        const postPrivacyDropdown = {
            display: 'flex',
            backgroundColor: 'rgb(70, 70, 70)',
            borderRadius: '5%',
            width: '35%'
        }

        const postPrivacyContainer = {
            display: 'flex',
            color: 'white',
            margin: '3px'
        }

        const postPrivacyIcon = {
            height: '12px',
            width: '12px'
        }

        const postPrivacyName = {
            color: 'white',
            marginLeft: '2px',
            fontSize: '10px',
            fontWeight: 'bold',
        }
        return (
            <div style={postProfile} >
                <img src='https://scontent.fbkk7-3.fna.fbcdn.net/v/t31.0-1/cp0/c0.0.80.80a/p80x80/16107455_1061155120676848_1876131052870528889_o.jpg?_nc_cat=100&_nc_sid=7206a8&_nc_ohc=eo4y2TWRSCAAX8nqM-H&_nc_ht=scontent.fbkk7-3.fna&_nc_tp=27&oh=4f59287032f1edf5a0b16b030ffd66c0&oe=5F9C0E6F' alt="" style={postProfileImg} />
                <div style={postProfileContainer}>
                    <div style={postProfileName}>Saharat Nasahachart</div>
                    <div style={postPrivacyDropdown}>
                        <div style={postPrivacyContainer}>
                            <img src="https://static.xx.fbcdn.net/rsrc.php/v3/y0/r/EV6ei3atxZg.png" alt="" style={postPrivacyIcon} />
                            <div style={postPrivacyName}>Public</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PostProfile;