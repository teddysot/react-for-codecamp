import React, { Component } from 'react';

class PostAttach extends Component {
    render() {
        const postAttachContainer = {
            display: 'flex',
            justifyContent: 'center',
            height: '15%',
            marginTop: '10px'
        }

        const postAttachBox = {
            display: 'flex',
            alignItems: 'center',
            backgroundColor: '#222222',
            borderRadius: '5px',
            width: '95%',
            border: '1px solid #4a4a4a'
        }

        const postAttachTitle = {
            color: 'whitesmoke',
            fontWeight: '500',
            marginLeft: '10px'
        }

        return (
            <div style={postAttachContainer} >
                <div style={postAttachBox}>
                    <div style={postAttachTitle}>Add to Your Post</div>
                </div>
            </div>
        );
    }
}

export default PostAttach;