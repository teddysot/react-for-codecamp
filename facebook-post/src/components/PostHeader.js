import React, { Component } from 'react';

class PostHeader extends Component {
    render() {
        const postHeader = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '15%',
            borderBottom: 'solid 1px #4a4a4a'
        }

        const postTitle = {
            fontSize: '20px',
            fontWeight: 'bold',
            color: 'white'
        }
        return (
            <div style={postHeader}>
                <div style={postTitle}>Create Post</div>
            </div>
        );
    }
}

export default PostHeader;