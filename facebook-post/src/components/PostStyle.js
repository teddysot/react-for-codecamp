import React, { Component } from 'react';

class PostStyle extends Component {
    render() {

        const postStyleContainer = {
            display: 'flex'
        }

        // const postStyleIcon = {
        //     display: 'flex',
        //     marginLeft: '5px'
        // }
        // const postStyleIcon: hover = {
        //     cursor: 'pointer'
        // }
        return (

            <div style={postStyleContainer}>
                <img className="post-style-icon" src="https://www.facebook.com/images/composer/SATP_Aa_square-2x.png" height="38" alt="" />
            </div>
        );
    }
}

export default PostStyle;