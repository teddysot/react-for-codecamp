import React from 'react';
import './App.css';

import PostHeaderComponent from './components/PostHeader'
import PostProfileComponent from './components/PostProfile'
import PostTextBoxComponent from './components/PostTextBox'
import PostStyleComponent from './components/PostStyle'
import PostAttachComponent from './components/PostAttach'
import PostButtonComponent from './components/PostButton'

function App() {
  const appStyle = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh'
  }
  const container = {
    display: 'flex',
    flexDirection: 'column',
    width: '500px',
    height: '400px',
    backgroundColor: '#282828',
    borderRadius: '5%',
  }
  return (
    <div className="App" style={appStyle}>
      <div style={container}>
        <PostHeaderComponent />
        <PostProfileComponent />
        <PostTextBoxComponent />
        <PostStyleComponent />
        <PostAttachComponent />
        <PostButtonComponent />
      </div>
    </div>
  );
}

export default App;
