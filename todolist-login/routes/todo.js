const router = require('express').Router();
const jwt = require('jsonwebtoken')
const todoController = require('../controllers/todo');
const db = require('../models');

const auth = async (req, res, next) => {
    try {
        const token = extractToken(req);
        const payload = jwt.verify(token, "teddysot");
        db.User.findOne({ where: { id: payload.id } })
            .then((targetUser) => {
                req.user = targetUser;
                next();
            })
            .catch((err) => {
                console.log(err);
                res.status(500).send({ message: "Internal Server Error" });
            });

    } catch (err) {
        console.log(err);
        res.status(401).send("Unauthorized");
    }
}

router.get('/', auth, todoController.getAllTodos)
router.get('/:id', todoController.getTodoById)
router.post('/', todoController.createTodo)
router.put('/:id', todoController.updateTodo)
router.delete('/:id', todoController.deleteTodo)

module.exports = router

function extractToken(req) {
    return req.headers["authorization"].split(' ')[1];
}
