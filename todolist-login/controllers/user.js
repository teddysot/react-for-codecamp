const db = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

exports.login = async (req, res) => {
    const { username, password } = req.body
    const targetUser = await db.User.findOne({ where: { username } })

    if (!targetUser) {
        res.status(400).send({ message: "Not found Username" })
    }
    else {
        if (bcrypt.compareSync(password, targetUser.password)) {
            const token = jwt.sign({ id: targetUser, name: targetUser.name }, "teddysot", { expiresIn: 3600 })
            res.status(200).send({ token })
        }
        else {
            res.status(400).send({ message: "Incorrect Password" })
        }
    }
}

exports.register = async (req, res) => {
    const { username, password, name } = req.body

    const targetUser = await db.User.findOne({ where: { username } })

    if (targetUser) {
        res.status(400).send({ message: "Username already taken" })
    }
    else {
        const salt = bcrypt.genSaltSync(12)
        const hashedPassword = bcrypt.hashSync(password, salt)

        await db.User.create({
            username,
            name,
            password: hashedPassword
        })

        res.status(201).send({ message: "User created successfully" })
    }
}