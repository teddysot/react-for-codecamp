const express = require('express')
const db = require('./models')
const app = express();
const userRoutes = require('./routes/user')
const todoRoutes = require('./routes/todo')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/user', userRoutes)
app.use('/todos', todoRoutes)

app.listen(80, () => {
    console.log("Server is running at port: 80");
})

db.sequelize.sync().then(() => {
    console.log("Database established");
})