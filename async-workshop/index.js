const fs = require("fs");


const filePromise = (fileName) => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, "utf-8", (error, data) => {
            if (error) {
                reject(error)
            }
            else {
                resolve(data);
            }
        })
    })
}

filePromise("sonter.txt")
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.log(err);
    })

let money = 500
const eatBuffet = () => {
    return new Promise((resolve, reject) => {
        if (money > 80) {
            resolve("ดีใจจัง");
        }

        reject("เสียใจจัง");
    });
}

eatBuffet()
    .then((message) => {
        console.log(`eatBuffet Resolve ${message}`);
    }).catch((message) => {
        console.log(`eatBuffet Reject ${message}`);
    });

const powRange = (base, index, result) => {
    return new Promise((resolve, reject) => {
        if (base ** index === result) {
            resolve("Correct")
        }
        reject("Incorrect")
    })
}

powRange(2, 3, 9)
    .then((message) => {
        console.log(message);
    })
    .catch((message) => {
        console.log(message);
    })

const logAndLor = (alphabet, time) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(alphabet);
            resolve();
        }, time)
    })
}

// logAndLor("A", 1000)
//     .then(() => {
//         return logAndLor("B", 1000)
//     })
//     .then(() => {
//         return logAndLor("C", 1000)
//     })
//     .then(() => {
//         return logAndLor("D", 1000)
//     })
//     .then(() => {
//         return logAndLor("E", 1000)
//     })
//     .then(() => {
//         return logAndLor("F", 1000)
//     })

const runAlphabet = async () => {
    await logAndLor("A", 1000)
    await logAndLor("B", 1000)
    await logAndLor("C", 1000)
    await logAndLor("D", 1000)
    await logAndLor("E", 1000)
    await logAndLor("F", 1000)
}

runAlphabet();